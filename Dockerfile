FROM debian:stable-slim

# renovate: datasource=github-releases depName=morpheus65535/bazarr versioning=semver-coerced
ENV BAZARR_VER=1.5.1

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8

RUN set -feux \
  && echo '\nAPT::Install-Recommends "false";\nAPT::Install-Suggests "false";\n' >> /etc/apt/apt.conf \
  && sed -i 's/main/main contrib non-free non-free-firmware/' /etc/apt/sources.list.d/debian.sources \
  && apt update \
  && apt install -y -q curl dumb-init python3-dev python3-pip python3-distutils python3-venv unrar unzip \
  && curl -sL https://github.com/morpheus65535/bazarr/releases/download/v${BAZARR_VER}/bazarr.zip -o bazarr.zip \
  && mkdir -p /opt/bazarr \
  && unzip bazarr.zip -d /opt/bazarr \
  && rm -f bazarr.zip \
  && cd /opt/bazarr \
  && python3 -m venv . \
  && ./bin/python3 -m pip install -r requirements.txt \
  && chown -R 1664:1664 /opt/bazarr \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/bazarr

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["./bin/python3", "bazarr.py", "--config", "/config"]
